import React from 'react';
import logo from './logo.svg';
import './App.css';

import Box from "@material-ui/core/Box"
import Calendar from "./components/calendar"
import LeftBar from "./components/left-bar"
import { makeStyles } from "@material-ui/core/styles"
import Form from './components/form';

const useStyles = makeStyles({
})

function App() {


  return (

    <Box display="flex">
      <Box display="flex" width={"100px"}>
        <LeftBar />
      </Box>

      <Box width={1} flexDirection="column" justifyContent={"center"} alignItems="center" display="flex">
        <Form />
        <Calendar />
      </Box>
    </Box>
  );
}

export default App;

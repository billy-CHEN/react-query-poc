import { useCallback } from "react"
import { useQuery } from "react-query"
import { useDispatch, useSelector } from "react-redux"
import axios from "axios"
import { select } from '../store/pokemonSlice'


const usePokemons = () => {

    const dispatch = useDispatch()
    const selectedPokemon = useSelector<any>(s => s.pokemon.selected)

    const onClick = useCallback((id: number) => {
        dispatch(select(id))
    }, [dispatch])

    const { data, isLoading, isError } = useQuery("pokemons", () => axios.get("http://localhost:1337/pokemons").then(res => res.data), {
        refetchOnWindowFocus: false,
        onSuccess: () => { },
        onError: () => { }
    })

    return {
        onClick,
        data,
        isLoading,
        isError,
        selectedPokemon
    }


}

export default usePokemons
import { useMutation, useQueryClient } from "react-query"
import axios from "axios"

const useUpdatePokemon = () => {
    const queryClient = useQueryClient()
    return useMutation((values: any) => axios.put(`http://localhost:1337/pokemons/${values.id}`, { name: values.name }).then(res => res.data), {
        onSuccess: (values) => {
            queryClient.setQueryData(['pokemon', values.id], values)
            queryClient.invalidateQueries("pokemons")
        },
        onError: () => {
        },
        onSettled: () => { },
    })
}

export default useUpdatePokemon
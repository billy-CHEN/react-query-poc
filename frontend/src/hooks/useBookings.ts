import axios from "axios"
import { useQuery } from "react-query"
import { useSelector } from "react-redux"

const useBookings = () => {
    const selectedPokemon = useSelector<any>(s => s.pokemon.selected)

    const { data, isLoading, isError } = useQuery("bookings", () => axios.get("http://localhost:1337/bookings").then(res => res.data), {
        refetchOnWindowFocus: false,
    })

    return {
        selectedPokemon,
        data,
        isLoading,
        isError
    }
}

export default useBookings
import { useQuery } from "react-query"
import axios from "axios"

const usePokemon = (id: string) => {
    return useQuery(["pokemon", id], () => axios.get(`http://localhost:1337/pokemons/${id}`).then(res => res.data), {
        refetchOnWindowFocus: false,

        onSuccess: () => { }
    })
}

export default usePokemon
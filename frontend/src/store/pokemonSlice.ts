import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    selected: null,
}

export const pokemonSlice = createSlice({
    name: 'pokemon',
    initialState,
    reducers: {
        select: (state, action) => {
            state.selected = action.payload
        },
    },
})

// Action creators are generated for each case reducer function
export const { select } = pokemonSlice.actions

export default pokemonSlice.reducer
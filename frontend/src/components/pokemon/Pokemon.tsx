import React from "react"

import usePokemon from "../../hooks/usePokemon"


import Box from "@material-ui/core/Box"
import { makeStyles } from "@material-ui/core/styles"
import { useSelector } from "react-redux"

type Props = {
    id: string,
    img: string;
    name: string
    isSelected?: boolean | null
}

const useStyles = makeStyles({
    root: {
        backgroundColor: ({ isSelected }: any) => isSelected === false ? "inherit" : "lightgrey"
    }
})

const Pokemon = ({ id, img, name, isSelected }: Props) => {
    const classes = useStyles({ isSelected })

    return (
        <Box flexDirection="column" className={classes.root} display="flex" justifyContent="center" alignItems="center">
            <img width="80" src={img} alt={id} />
            {name}
        </Box>
    )
}

// export default Pokemon
export default Pokemon
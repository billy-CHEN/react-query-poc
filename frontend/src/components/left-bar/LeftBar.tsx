import React from "react"
import usePokemons from "../../hooks/usePokemons"

import { makeStyles } from "@material-ui/core/styles"
import Box from "@material-ui/core/Box"
import Pokemon from "../pokemon"



const useStyles = makeStyles({
    leftbar: {
        backgroundColor: "white",
        boxShadow: "1px 0px 2px gray",
        height: "100vh"
    }
})

const LeftBar = () => {
    const classes = useStyles()

    const { selectedPokemon, data, isLoading, isError, onClick } = usePokemons()

    if (isLoading) return <div>Loading...</div>
    if (isError) return <div>Error</div>

    return (
        <Box flexDirection="column" className={classes.leftbar} display="flex" width={1}>
            {data && data.map((d: any, i: any) => (
                <Box onClick={() => onClick(d.id)}>
                    <Pokemon
                        id={d.id}
                        img={d.img}
                        name={d.name}
                        isSelected={selectedPokemon === d.id} />
                </Box>
            ))}
        </Box>
    )
}

export default LeftBar
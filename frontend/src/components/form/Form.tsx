import React, { useState } from "react"

import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import { useSelector } from "react-redux"
import usePokemon from "../../hooks/usePokemon"
import useUpdatePokemon from "../../hooks/useUpdatePokemon"

const Form = () => {
    const [name, setName] = useState("")

    const id: any = useSelector<any>(s => s.pokemon.selected)
    const { data, isLoading } = usePokemon(id)
    const mutation = useUpdatePokemon()

    const save = () => {
        mutation.mutate({ id, name })
    }

    return (
        <Box m={3} display="flex" alignItems="space-between">
            {data ?
                (<TextField onChange={(e) => setName(e.target.value)} key={`text-field-${data.id}`} id="standard-required" label="Name" defaultValue={data ? data.name : ""} />)
                : (<TextField key={`text-field`} id="standard-required" label="Name" defaultValue={""} />)
            }
            <Button variant="contained" color="primary" onClick={save}>Save</Button>
        </Box>
    )
}

export default Form
import React from "react"
import { useSelector } from "react-redux"

import useBookings from "../../hooks/useBookings"

import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import { makeStyles } from "@material-ui/core/styles"

import Pokemon from "../pokemon"
import CalendarCell from "./CalendarCell"

const useStyles = makeStyles({
    grid: {
        "&>div": {
            border: "1px solid black"
        }
    },
})

const Calendar = () => {

    const classes = useStyles()

    const { selectedPokemon, data, isLoading, isError } = useBookings()

    if (isLoading || isError || !data) return null

    return (
        <Box display="flex" width={3 / 4}>
            <Grid className={classes.grid} container justifyContent="center" alignContent="center" alignItems="center">
                {data.map((d: any, i: number) => (
                    <Grid key={`grid-${d.pokemon.id}-${i}`} justifyContent="center" alignItems="center" item xs={4}>
                        <CalendarCell pokemonId={d.pokemon.id} isSelected={selectedPokemon === d.pokemon.id} />
                    </Grid>
                ))}
            </Grid>
        </Box>
    )

}

export default Calendar
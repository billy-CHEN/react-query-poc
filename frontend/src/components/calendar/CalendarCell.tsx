import React from "react"
import usePokemon from "../../hooks/usePokemon"
import usePokemons from "../../hooks/usePokemons"
import Pokemon from "../pokemon"

type Props = {
    pokemonId: string
    isSelected: boolean
}

const CalendarCell = ({ pokemonId, isSelected }: Props) => {
    const { data, isLoading, isError } = usePokemon(pokemonId)

    if (isLoading || isError) return null

    return (
        <Pokemon id={data.id} img={data.img} name={data.name} isSelected={isSelected} />
    )

}

export default React.memo(CalendarCell)